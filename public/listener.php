<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php

$response = "";

if (isset($_REQUEST["whole_sum"]) && isset($_REQUEST["map"]) && isset($_REQUEST['personal_balances']) &&
    isset($_REQUEST['event_id']) && isset($_REQUEST['names']) && isset($_REQUEST['new_budget'])){

    $balance = $_REQUEST["whole_sum"];
    $map = $_REQUEST["map"];
    $personal_balances = $_REQUEST['personal_balances'];
    $event_id = $_REQUEST['event_id'];
    $new_budget = $_REQUEST['new_budget'];

    $for_insert = array();

    if ((count($_REQUEST["map"])+1) < count($_REQUEST['personal_balances'])){

        for ($i = (count($_REQUEST["map"])+1); $i < count($_REQUEST['personal_balances']); $i++){
                $for_insert[$_REQUEST['names'][$i]] = $_REQUEST['personal_balances'][$i];
        }
    }

    if (update_participants_data($event_id, $map, $personal_balances, $balance, $for_insert) == 'Success'){
        $data = json_encode(get_participants_data($event_id));
        $response = $data;
    }

    change_budget($event_id, $new_budget);
}

if (isset($_REQUEST['id_for_del_event'])){
    $ev_id_for_del = $_REQUEST['id_for_del_event'];

    $response = delete_event($ev_id_for_del);
}

if (isset($_REQUEST['del_part_id']) && isset($_REQUEST['event_id']) && isset($_REQUEST['new_budget'])){
    $event_id = $_REQUEST['event_id'];
    $del_part_id = $_REQUEST['del_part_id'];
    $new_budget = $_REQUEST['new_budget'];

    $response = delete_participant($del_part_id);
    $response .= change_budget($event_id, $new_budget);
}

echo $response;

?>