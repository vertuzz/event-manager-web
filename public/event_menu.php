<?php require_once("../includes/db_connection.php"); ?>
<?php include("../templates/header.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php $event_id = htmlspecialchars($_GET['ev_id']);?>

<?php
$event_data = mysqli_fetch_assoc(get_event_data($event_id));
$participants_data = get_participants_data($event_id);
?>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <h2>Мероприятие: <?php echo $event_data['name']; ?></h2>
            <p>Здесь выведены имена участников и их задолженность:</p>
        </div>
    </div>
    <table class="table table-hover" id="part-list">
        <thead>
        <tr>
            <th>№</th>
            <th>Участник</th>
            <th>Долг</th>
            <th>Внесение средств</th>
        </tr>
        </thead>
        <tbody id="table">
        <?php $temp_id = 1;
        $map = array();
        foreach($participants_data as $person){ ?>
        <tr id="<?php echo $temp_id; ?>-row">
            <td><?php echo $temp_id; ?></td>
            <td id="<?php echo $temp_id; ?>-names"><?php echo $person[2]; ?> <img src="images/krestik.png" title="Удалить"
                id="<?php echo $temp_id; ?>-del" onclick="del_participant(<?php echo $temp_id; ?>)"></td>
            <td id="<?php echo $temp_id; ?>-debt"><?php echo $person[3]; ?></td>
            <td><input class="form-control" id="<?php echo $temp_id?>-input" type="text" value=""></td>
        </tr>
        <?php
            $map[$temp_id] = $person[0];
            $temp_id++;
        } ?>
        <tr>
            <td></td>
            <td>Итоговая задолженность:</td>
            <td id="whole-sum"></td>
            <td><button type="button" onclick="pageChange()" class="btn btn-primary">Подтвердить</button></td>
        </tr>
        </tbody>
    </table>
    <div><button type="button" class="btn btn-primary"
                 data-toggle="modal" data-target="#addMenu">Добавить участника</button></div>

    <!-- Modal -->
    <div id="addMenu" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Меню добавления участника</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="new_name">Имя:</label>
                        <input type="text" class="form-control" id="new_name">
                    </div>
                    <form role="form" onchange="showSumForm()">
                        <div class="radio">
                            <label><input type="radio" checked="checked"
                                          name="optradio" value="1">Распределить бюджет между участниками</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="optradio" value="2">Сохранить общий первоначальный взнос,
                                увеличив бюджет</label>
                        </div>
                    </form>
                    <div id="formForSum" style="display: none">
                        <label for="new_sum">Сумма:</label>
                        <input type="text" class="form-control"
                               value="<?php echo round($event_data['budget']/ ($temp_id-1),2);?>" id="new_sum">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="add_part" data-dismiss="modal">Добавить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>

        </div>
    </div>
    <?php //print_r($map); ?>
    <div align="center"><button type="button" id="send_data" class="btn btn-success btn-lg">Записать</button></div>
    <div align="center"><h2 id="alert" style="display: none; background: #5cb85c; padding: 50px;">Успех</h2></div>
<script>
    var event_id = <?php echo $event_id;?>;
    var budget = parseFloat("<?php echo $event_data['budget'];?>");
    var index = document.getElementById('part-list').getElementsByTagName("tr").length - 2;
    var map = <?php echo json_encode($map) ?>;

    pageChange();

    function pageChange(add = false) {
        var amount_from_db, sum;
        var map_debt = [];
        var map_income = [];

        amount = document.getElementById('part-list').getElementsByTagName("tr").length - 2;
        sum = 0;

        for (i = 1; i < amount + 1; i++) {
            map_debt[i] = parseFloat(document.getElementById(i + "-debt").innerHTML);
            if(isNaN(parseFloat(document.getElementById(i + "-input").value))){
                map_income[i] = 0;
            }else {
                map_income[i] = parseFloat(document.getElementById(i + "-input").value);
            }
        }
        for (i = 1; i < amount + 1; i++) {
            if(add == true){
                if (i == amount) break;
                document.getElementById(i + "-debt").innerHTML = (map_debt[i] -
                    parseFloat(document.getElementById(amount + "-debt").innerHTML) / (amount-1)).toFixed(2);
            }else {
                (document.getElementById(i + "-debt").innerHTML = map_debt[i] - map_income[i]).toFixed(2);
            }
            sum += (map_debt[i] - map_income[i]);
            document.getElementById(i + "-input").value = "";
        }
        document.getElementById("whole-sum").innerHTML = sum.toFixed(2);
    }

    $(document).ready(function(){
        $("#send_data").click(function(){
            var temp_id = document.getElementById('part-list').getElementsByTagName("tr").length - 1;
            var balances = [];
            var names = [];
            for (i = 1; i < temp_id; i++){
                balances[i] = document.getElementById(i + "-debt").innerHTML;
                names[i] = document.getElementById(i + "-names").innerHTML;
            }
            $.post("/listener.php",
                {
                    whole_sum: document.getElementById("whole-sum").innerHTML,
                    map: map,
                    new_budget: budget,
                    personal_balances: balances,
                    names: names,
                    event_id: event_id
                },
                function(data,status){
                    if (status == 'success'){
                        var response = JSON.parse(data);
                        var map = [];
                        var num_of_part = document.getElementById('part-list').getElementsByTagName("tr").length - 2;
                        for (i = 1; i < num_of_part+1; i++){
                            map[i] = response[i-1][0];
                        }
                        $("#alert").slideDown(300).delay(800).fadeOut(300);
                    }else {
                        $("#alert").text('Неудача').slideDown(300).delay(800).fadeOut(300);
                        alert(data +" " + status);
                    }

                });
        });
    });

    function showSumForm(){
        var tip =  document.querySelector('[name="optradio"]:checked').value;
        if(tip == 1){
            document.getElementById('formForSum').style.display = "none";
        }else {
            document.getElementById('formForSum').style.display = "block";
        }

    }

    $(document).ready(function(){
        var index = document.getElementById('part-list').getElementsByTagName("tr").length - 1;
        $('#add_part').click(function(){
            var new_part_name = document.getElementById('new_name').value;
            var opt = document.querySelector('[name="optradio"]:checked').value;
            var sum, recount;
            if (opt == 1){
                sum = budget / index;
                recount = true;
            }else {
                sum = parseFloat(document.getElementById('new_sum').value);
                budget += sum;
                recount = false;
            }
            $("<tr id='"+ index +"-row'><td>"+ index +"</td><td id='"+ index +"-names'>"+ new_part_name +" " +
                "</td><td id='"+ index +"-debt'>"+ sum.toFixed(2) +"</td><td>" +
                "<input class=\"form-control\" id=\""+ index +"-input\" " +
                "type=\"text\" value=\"\"></td></tr>").insertBefore($("tr:last"));
            index++;
            document.getElementById('new_name').value = "";
            document.getElementById('new_sum').value = sum.toFixed(2);
            pageChange(recount);
        })
    });

    function del_participant(i){
        var new_budget = budget - (budget/index);
        if (confirm('Вы уверены, что хотите удалить участника?')) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    //alert(xmlhttp.responseText);
                    location.reload();
                }
            };
            xmlhttp.open("POST", "/listener.php", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send("del_part_id=" + map[i] + "&event_id=" + event_id + "&new_budget=" + new_budget);
        }
    }

</script>

<?php include("../templates/footer.php"); ?>
