<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php if(isset($_POST['save'])) {
    $event_name = htmlspecialchars($_POST['event_name']);
    $budget = htmlspecialchars($_POST['budget']);
    $participants = explode(",", htmlspecialchars($_POST['participants']));
    $participants = array_map('trim', $participants);
    $option = $_POST['optionsRadios'];
    $event_id = create_event($event_name,$budget, $option, $participants);
    $redirect_url = "event_menu.php/?ev_id=".$event_id;
    header('Location: http://'. $_SERVER['HTTP_HOST'].'/'.$redirect_url);
}else{
?>
<?php include("../templates/header.php"); ?>
<div class="container">
    <h2 align="center">Введите данные для создания нового мероприятия</h2>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
        <div class="form-group">
            <label for="event_name">Название мероприятия</label>
            <input type="text" class="form-control" id="event_name" name="event_name" accept-charset="utf-8">
            <div class="row">
                <div class="col-md-6">
                    <label for="budget">Бюджет</label>
                    <input type="text" class="form-control" id="budget" name="budget">
                </div>
            </div>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                Бюджет на мероприятие в целом
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                Бюджет в расчете на одного учасника
            </label>
        </div>
        <p>Введите всех учасников, разделив их запятой</p>
        <textarea class="form-control" name="participants" rows="3"></textarea><br>
        <button type="submit" class="btn btn-default" name="save">Создать</button>
    </form>
</div>
<?php } ?>

<?php include("../templates/footer.php"); ?>
