<?php require_once("../includes/db_connection.php"); ?>
<?php include("../templates/header.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<div class="container">
    <div class="jumbotron">
        <h2>Добро пожаловать в Event manager!</h2>
        <p>Тут вы сможете с легкостью организовать любое мероприятие</p>
        <p align="center"><a class="btn btn-primary btn-lg" href="/new_event.php" role="button">Создать мероприятие</a></p>
    </div>
</div>
<div class="container">
    <h2 align="center">Список ваших мероприятий</h2>
    <?php //echo "<pre>"; $events_list = get_events_list(); print_r(get_events_list()); echo "</pre>"; ?>
    <?php $events_list = get_events_list();
    $map = array();
    ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>№</th>
            <th>Имя мероприятия</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody id="table">
        <?php $temp_id=1;
        foreach ($events_list as $event){ ?>
        <tr id="<?php echo $temp_id; ?>-row">
            <td><?php echo $temp_id; ?></td>
            <td><a href="event_menu.php?ev_id=<?php echo $event['0']?>"><?php echo $event['1']?></a></td>
            <td><button type="button" onclick="delete_event(<?php echo $temp_id; ?>, <?php echo $event['0']?>)"
                        class="btn btn-danger btn-xs">Удалить</button></td>
        </tr>
        <?php $map[$temp_id] = $event['0'];
            $temp_id++;} ?>
        </tbody>
    </table>

</div>
<script>
    function delete_event(temp_id, event_id){
        if (confirm('Вы уверены, что хотите удалить мероприятие?')) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    //alert(xmlhttp.responseText);
                    var parent = document.getElementById("table");
                    var child = document.getElementById(temp_id + "-row");
                    parent.removeChild(child);
                }
            };
            xmlhttp.open("POST", "/listener.php", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send("id_for_del_event=" + event_id);
        }

    }


</script>
<?php include("../templates/footer.php"); ?>
