<?php
define("DB_SERVER", "localhost");
define("DB_USER", "event");
define("DB_PASS", "event");
define("DB_NAME", "event");

// create a database connection;
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS,
    DB_NAME);
// Test if connection occurred
if (mysqli_connect_errno()){
    die("Database connection failed: " .
        mysqli_connect_error() .
        " (" . mysqli_connect_errno() . ")"
    );
}
?>