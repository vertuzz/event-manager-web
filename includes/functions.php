<?php

function confirm_query($result_set){
    if(!$result_set){
        die("Database query failed.");
    }
}

function error_handler($connection, $query){
    if (!mysqli_query($connection, $query)) { // Проверка на ошибки
        echo "Error: " . $query . "<br>" . mysqli_error($connection);
    }
}

function create_event($event_name, $budget, $option, $participants){
    /*
     * Функция создания мероприятия исходя из значений переданных через форму
     */
    global $connection; //Соединение лежит в (includes/db_connection.php)

    $safe_event_name = mysqli_escape_string($connection ,$event_name); // Экранированное название мероприятия
    $safe_budget = (float)mysqli_escape_string($connection ,$budget);  // Экранированный бюджет
    if($option == 'option1'){ // Вариант с бюджетом на все мероприятие
        // Бюджет на участника рассчитывается как частное от деление общего на кол-во участников
        $budget_per_person = round($safe_budget / count($participants), 2);
    }else{ //Вариант с бюджетом в расчете на одного участника
        $budget_per_person = $safe_budget; // Прямая передача бюджета на участника из формы
        $safe_budget *= count($participants); // Бюджет на мероприятие - произведение кол-ва участников
                                                // на бюджет на человека
    }

    // Sql запрос для создания записи в таблице мероприятий
    $query_event = "INSERT INTO events (name, debt, budget) ";
    $query_event .= "VALUES ('{$safe_event_name}', '{$safe_budget}', '{$safe_budget}')";

    error_handler($connection, $query_event); // Проверка на ошибки

    $insert_id = mysqli_insert_id($connection); // Получение возвращаемого id записи

    // Sql запрос для создания записей в таблице участников с соотвтетсвующим id мероприятия
    $query_participants = "INSERT INTO participants (event_id, name, balance) VALUES ";
    foreach ($participants as $participant){ // Цикл записи всех участников в таблицу одним запросом
        $safe_participant = mysqli_escape_string($connection, $participant); // Экранирование
        // Конкатенация запроса
        $query_participants .= "({$insert_id}, '{$safe_participant}', {$budget_per_person}), ";
    }
    // Замена последних двух симоволов на ";"
    $query_participants = substr($query_participants, 0, -2) . ";";

    error_handler($connection, $query_participants); // Проверка на ошибки

    return $insert_id; // Возврат id Мероприятия
}

function get_events_list(){
    global $connection;

    $query = "SELECT * FROM events";
    $events_list_data = mysqli_query($connection, $query);
    confirm_query($events_list_data);
    return mysqli_fetch_all($events_list_data);
}

function get_event_data($event_id){
    global $connection;

    $query = "SELECT * FROM events WHERE id = {$event_id}";
    $event_data = mysqli_query($connection, $query);
    confirm_query($event_data);
    return $event_data;
}

function get_participants_data($event_id){
    global $connection;

    $query = "SELECT * FROM participants WHERE event_id = {$event_id}";
    $participants_set = mysqli_query($connection, $query);
    confirm_query($participants_set);
    $result = mysqli_fetch_all($participants_set);
    return $result;
}

function update_participants_data($event_id, $map, $personal_balances, $whole_balance, $for_insert){
    global $connection;

    for($i=1; $i<count($map)+1; $i++){
        $query = "UPDATE participants SET balance={$personal_balances[$i]} WHERE id={$map[$i]}";
        error_handler($connection, $query);
    }

    if (count($for_insert) > 0){
        foreach ($for_insert as $name=>$balance){
            $query2 = "INSERT INTO participants (event_id, name, balance) VALUES ({$event_id}, '{$name}', {$balance})";
            error_handler($connection, $query2);
        }

    }

    $query_balance = "UPDATE events SET debt={$whole_balance} WHERE id={$event_id}";
    error_handler($connection, $query_balance);


    return 'Success';

}

function delete_event($event_id){
    global $connection;

    $query_event = "DELETE FROM events WHERE id={$event_id}";
    error_handler($connection, $query_event);
    $query_part = "DELETE FROM participants WHERE event_id={$event_id}";
    error_handler($connection, $query_part);
    mysqli_close($connection);
    return 'Success';
}

function change_budget($event_id, $new_budget){
    global $connection;

    $query = "UPDATE events SET budget={$new_budget} WHERE id={$event_id}";
    error_handler($connection, $query);

    return "Success";
}

function delete_participant($del_part_id){
    global $connection;
    $query = "DELETE FROM participants WHERE id={$del_part_id}";
    error_handler($connection, $query);

    return "Success";
}
?>